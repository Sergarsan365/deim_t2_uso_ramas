using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Playerspeed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Playerspeed * Vector3.forward);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Playerspeed * Vector3.back);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Playerspeed * Vector3.right);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Playerspeed * Vector3.left);
        }
    }
}
